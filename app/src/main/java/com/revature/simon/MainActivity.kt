package com.revature.simon

import android.media.AudioAttributes
import android.media.SoundPool
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Popup
import com.revature.simon.ui.theme.SimonTheme
import kotlinx.coroutines.delay
import java.util.*
import kotlin.collections.HashMap

var red = Color.Red
var blue = Color.Blue
var green = Color.Green
var yellow = Color.Yellow
var pattern = Pattern()



class MainActivity : ComponentActivity() {
    var attr =  AudioAttributes.Builder ().setUsage( AudioAttributes.USAGE_GAME)
    .setContentType( AudioAttributes.CONTENT_TYPE_MUSIC).build()
    var sound:SoundPool = SoundPool.Builder().setMaxStreams(4).setAudioAttributes(attr).build()
    var soundMap = HashMap<Int, Int>()
    var timeState:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        soundMap[1] = sound.load(this, R.raw.sound1_1, 1)
        soundMap[2] = sound.load(this, R.raw.sound2_1, 1)
        soundMap[3] = sound.load(this, R.raw.sound3_1, 1)
        soundMap[4] = sound.load(this, R.raw.sound4_1, 1)
        soundMap[5] = sound.load(this, R.raw.sound5_1, 1)
        setContent {
            SimonTheme {
                Surface(color = MaterialTheme.colors.background) {
                    SimonButtons(sound, soundMap)
                }

            }
        }

    }

    @Composable
    fun timer()
    {
        var isTimerRunning by remember {mutableStateOf(timeState)}
        var redraw by remember { mutableStateOf(false)}

        LaunchedEffect(isTimerRunning)
        {
            while(isTimerRunning)
            {
                delay(500L)
                var x = pattern.getNext()
                if(x == -1)
                    isTimerRunning = false
                else {
                    Log.i("Pattern", "id: $x")
                    when(x){
                        1 -> {
                            Log.i("Color", "$red")
                            red = Color.Black
                            redraw = true
                            sound.play(soundMap[1] ?:0, 1F, 1F, 0, 0, 1F)
                        }
                        2 -> {
                            blue = Color.Black
                            redraw = true
                            sound.play(soundMap[2] ?:0, 1F, 1F, 0, 0, 1F)
                        }
                        3 -> {
                            yellow = Color.Black
                            redraw = true
                            sound.play(soundMap[3] ?:0, 1F, 1F, 0, 0, 1F)
                        }
                        4 -> {
                            green = Color.Black
                            redraw = true
                            sound.play(soundMap[4] ?:0, 1F, 1F, 0, 0, 1F)
                        }
                        5 -> {  red = Color.Red
                                blue = Color.Blue
                                yellow = Color.Yellow
                                green = Color.Green
                                redraw = false}
                    }
                }
            }
        }
        if(redraw)
            SimonButtons(soundPool = sound, soundMap, 1)
    }

    @Composable
    fun SimonButtons(soundPool: SoundPool, soundMap:Map<Int, Int>, showState: Int = 0){
        var show = remember{ mutableStateOf(showState)}
        var time = remember{ mutableStateOf(false)}

        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center){
            Button(onClick = {
                Log.i("Color", "Red")
                soundPool.play(soundMap[1] ?:0, 1F, 1F, 0, 0, 1F)
                if(!pattern.checkNext(1)){
                    show.value = 2
                    time.value = false
                }
                else if(pattern.isLast()){
                    time.value = true
                    pattern.push()
                }
//                timeState = false
//                timeState = true
            },
                modifier = Modifier.size(125.dp),
                shape = CircleShape,
                colors = ButtonDefaults.buttonColors(backgroundColor = red)){
            }

            Row(modifier = Modifier.fillMaxWidth(),horizontalArrangement = Arrangement.SpaceAround){

                Button(
                    onClick = {
                        Log.i("Color", "Green")
                        soundPool.play(soundMap[2] ?:0, 1F, 1F, 0, 0, 1F)
                        if(!pattern.checkNext(4)){
                            show.value = 2
                            time.value = false
                        }
                        else if(pattern.isLast()){
                            time.value = true
                            pattern.push()
                        }
//                        timeState = false
//                        timeState = true
                    },
                    modifier = Modifier.size(125.dp),
                    shape = CircleShape,
                    colors = ButtonDefaults.buttonColors(backgroundColor = green)
                ) {

                }
                Button(
                    onClick = {
                        Log.i("Color", "Blue")
                        soundPool.play(soundMap[3] ?:0, 1F, 1F, 0, 0, 1F)
                        if(!pattern.checkNext(2)){
                            show.value = 2
                            time.value = false
                        }
                        else if(pattern.isLast()){
                            time.value = true
                            pattern.push()
                        }
//                        timeState = false
//                        timeState = true
                    },
                    modifier = Modifier.size(125.dp),
                    shape = CircleShape,
                    colors = ButtonDefaults.buttonColors(backgroundColor = blue)
                ) {

                }
            }
            Button(
                onClick = {
                    Log.i("Color", "Yellow")
                    soundPool.play(soundMap[4] ?:0, 1F, 1F, 0, 0, 1F)
                    if(!pattern.checkNext(3)){
                        show.value = 2
                        time.value = false
                    }
                    else if(pattern.isLast()){
                        time.value = true
                        pattern.push()
                    }
//                    timeState = false
//                    timeState = true
                },
                modifier = Modifier.size(125.dp),
                shape = CircleShape,
                colors = ButtonDefaults.buttonColors(backgroundColor = yellow)
            ) {

            }
            Spacer(modifier = Modifier.height(64.dp))

            if(show.value==0){
                Button(
                    onClick = {
                        show.value = 1
                        pattern.push()
                        timeState = true
                        Log.i("Timer", "timeState? $timeState")

                    },
                    modifier = Modifier.size(height = 50.dp, width = 100.dp)
                ) {
                    Text(text = "Start")
                }
            }
            else if(show.value==1){
                Button(onClick = { show.value = 2
                    timeState = false
                    Log.i("Timer", "timeState? $timeState")
                                 },
                    modifier = Modifier.size(height = 50.dp, width = 100.dp))
                {
                    Text(text = "End")
                }
            }
            else
            {
                Spacer(modifier = Modifier.height(50.dp))
            }
        }
        if(timeState)
            timer()
        if(time.value)
            timer()

        if(show.value==2)
            popUp(show)
    }

    @Composable
    fun popUp(showPopup:MutableState<Int>) {
        val popupWidth = 200.dp
        val popupHeight = 150.dp
        val cornerSize = 16.dp
        if (showPopup.value == 2)
            Popup(alignment = Alignment.Center) {
                Box(
                    modifier = Modifier
                        .size(popupWidth, popupHeight)
                        .background(Color.White, RoundedCornerShape(cornerSize))
                ) {
                    Column(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(text = "Score:", fontSize = 30.sp)

                        Text(
                            text = "${(pattern.outputSize()/2 - 1).coerceAtLeast(0)}",
                            fontSize = 30.sp
                        )

                        Spacer(modifier = Modifier.height(16.dp))

                        Button(onClick = {
                            showPopup.value = 0
                            pattern.clear()
                        }) {
                            Text(text = "Restart")
                        }
                    }
                }
            }
    }
}