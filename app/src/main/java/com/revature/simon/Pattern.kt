package com.revature.simon

import android.util.Log
import kotlin.random.Random

class Pattern {
    //stack-like: only can push objects onto stack or pop them off
    //or clear the whole thing
    var stack = ArrayList<Int>()
    var curIndex = -1
    var nextIndex = 0
    var last = false

    //This function returns false if the input was not accepted
    //If the input was not accepted, it generates a valid input
    fun push(num:Int=-1):Boolean
    {
        //if no number was provided or number was invalid,
        //then randomly generate a number and add it
        if(num <=0 || num>= 5)
        {
            var x = Random.nextInt(1,5)
            Log.i("Pattern", "Adding a random number($x) to stack")
            stack.add(x)
            stack.add(5)
            return false
        }

        //otherwise, add the number to the end of the stack
        Log.i("Pattern", "Adding $num to stack")
        stack.add(num)
        return true
    }
    fun pop(): Int {
        //if the stack is empty, return negative one
        if (stack.size < 1) {
            Log.i("Pattern", "Pattern is empty, returning -1")
            return -1
        }
        //otherwise remove the last item and return it
        Log.i("Pattern", "Popping value ${stack.last()} from stack")
        return stack.removeAt(stack.size - 1)
    }

    fun getNext():Int{

        if(nextIndex > stack.size - 1) {
            nextIndex = 0
            return -1
        }

        var x = stack[nextIndex]
        nextIndex++
        return x
    }

    fun clear()
    {
        Log.i("Pattern", "Now Clearing the pattern")
        stack.clear()
        resetIndex()
        last = false
        curIndex = -1
        nextIndex = 0
    }

    fun isLast():Boolean{

        return last
    }

    fun checkNext(check:Int):Boolean
    {
        Log.i("Pattern", "Checking if $check was the next input")

        //increase the index so we are checking the right value
        curIndex++

        if (stack[curIndex] == 5){
            curIndex++
        }

        Log.i("Pattern", "Checking if Out-Of-Bounds: Current Index: $curIndex; maxSize: ${stack.size}")

        //if we would go outside the bounds, fail and return false
        if(curIndex+1 >= stack.size||curIndex<-1)
            return false

        //store the result temporarily
        var x = stack[curIndex]==check
        Log.i("Pattern", "Checking if input matches stored value: input: $check; stored: ${stack[curIndex]}")

        //if the given and expected values don't match (if the player entered the wrong input)
        //then we reset the index
        if(!x) {
            resetIndex()
            Log.i("Pattern", "Values did not match, resetting the index")
        }

        if(curIndex >= stack.size - 2){
            last = true
            resetIndex()
        }
        else{
            last = false
        }

        //return the result of the matching
        return x
    }
    fun resetIndex()
    {
        Log.i("Pattern", "Now resetting the current Index")
        curIndex=-1
    }
    fun outputPattern():String
    {

        return stack.toString()
    }

    fun outputSize():Int{
        return stack.size
    }
}